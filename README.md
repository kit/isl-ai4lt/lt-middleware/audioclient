# Audioclient

How to use the audioclient:

1) Clone the repo
2) Install dependencies (e.g. python3, sseclient)

Through the parser specify arguments:

1) url: To which mediator to connect to (if authentication is required follow the steps the program outputs)
2) input source: Either -i ffmpeg -f audio_or_video_file or use -L to see which microphones are available and specify through -a (needs PyAudio to be installed). If the audio is sent to slowly use a larger chunk_size e.g. -c 32000 to send audio in one second chunks to prevent network overhead bottleneck
3) Use --output-file output.txt to write the output of the program to some file
4) Use --asr-kv language=? to specify a input language (from --list-available-languages) and mt workers --run-mt comma_separated_list_of_mt_languages (from --list-available-languages)
5) You can choose either the mode with no resending (default) or with resending by --asr-kv mode=SendUnstable --mt-kv mode=SendUnstable
6) You can either run in online or offline mode: Online is default, for offline do --ffmpeg-speed -1 (to send the audio as fast as possible, maybe also increase the chunk_size further) --asr-kv version=offline (for offline ASR there is also a better segmenter which can be used with --asr-kv segmenter=SHAS) --mt-kv version=offline --prep-kv version=offline --textseg-kv method=offline_model
7) Use --use-prep to use a preprocessing/noise cancellation model
8) If you just want the ASR/MT output and no textsegmentation (e.g. <br> elements) do --no-textsegmenter

Other things:

1) To show the session on the website do --show-on-website (and --website-title)
2) To upload a video to the archive use --upload-video and follow the instructions
3) Run --run-scheduler to start the scheduler based on rtmp_list.txt and sessions.txt

Custom sessions:

Setup:

1) first request a new session id (or use an already requested one)
2) then add a new stream to a session (or use a stream already added), with this different users can sent to different streams of the same session.
3) then set a graph (which component sends information to which other component). There can be multiple asr components, e.g. asr:0, asr:1, ...
4) then set the properties (for each component). Properties are used in StreamASR.py, StreamMT.py, ...

Using the custom session:

Constantly upload audio chunks (to the ltapi; best in a new thread)
and read the response (via the ltapi)

Finish a stream:

Send an END request (to the ltapi)
and wait until sseclient session ends
